#!/bin/bash -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function usage() {
    echo "Usage: $0 [OPTION] device"
    echo "Assembles boot.img from compiled kernel."
    echo "Removes old kernel modules from ramdisk, replace with specified ones."
    echo "Modifies deviceinfo_modules_initfs so modules are loaded automatically (disable with --no-module-load)"
    echo
    echo " -m, --modules=MODULES        comma-separated list of modules to use"
    echo " -p, --modules-pmaports       take module list from device package"
    echo " -e, --extra-modules=MODULES  comma-separated list of modules to append to existing list (to be used with --modules-pmaports)"
    echo " -o OUTDIR                    output directory"
    echo " --no-module-load             disable automatic loading of modules in ramdisk"
    echo " --hook=HOOK                  enable specified hook"
    echo " -h, --help                   show this help text"
}

LONGOPTS=modules-pmaports,modules:,extra-modules:,no-module-load,help,hook:
OPTIONS=pm:e:o:h

PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [ "$?" -ne 0 ]; then
    usage
    exit 2
fi
eval set -- "$PARSED"

arg_modules_pmaports=0
arg_modules=
arg_extra_modules=
no_module_load=0
modules=
hook=
KOUT=.
while true; do
    case "$1" in
        -p|--modules-pmaports)
            arg_modules_pmaports=1
            shift
            ;;
        -m|--modules)
            arg_modules="$2"
            shift 2
            ;;
        -e|--extra-modules)
            arg_extra_modules="$2"
            shift 2
            ;;
        -o)
            KOUT="$2"
            shift 2
            ;;
        --no-module-load)
            no_module_load=1
            shift
            ;;
        --hook)
            hook="$2"
            shift 2
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Arg error"
            usage
            exit 3
            ;;
    esac
done

if [ $# -ne 1 ]; then
    echo "Need to provide device name"
    usage
    exit 4
fi

if [ ! -f $KOUT/arch/arm/boot/zImage ] && [ ! -f $KOUT/arch/arm64/boot/Image.gz ]; then
    echo "Please run from the linux source tree with "$KOUT"/arch/arm/boot/zImage or "$KOUT"/arch/arm64/boot/Image.gz"
    exit 1
fi

device="$1"
pmaports_dir="$(pmbootstrap config aports)"
source "$pmaports_dir"/device/*/device-"$device"/deviceinfo

# Process more arguments
if [ "$arg_modules_pmaports" -eq 1 ]; then
    if [ -n "$deviceinfo_modules_initfs_mainline" ]; then
        modules="$deviceinfo_modules_initfs_mainline"
    elif [ -n "$deviceinfo_modules_initfs" ]; then
        modules="$deviceinfo_modules_initfs"
    else
        echo "Failed to find deviceinfo_modules_initfs"
        exit 5
    fi
fi
if [ -n "$arg_modules" ]; then
    modules="${arg_modules//,/ }"
fi
if [ -n "$arg_extra_modules" ]; then
    modules="$modules ${arg_extra_modules//,/ }"
fi

dtb=""
if [ -n "$deviceinfo_dtb_mainline" ]; then
    dtb="$deviceinfo_dtb_mainline"
elif [ -n "$deviceinfo_dtb" ]; then
    dtb="$deviceinfo_dtb"
else
    echo "Couldn't find deviceinfo_dtb variable"
    exit 1
fi

case "$deviceinfo_arch" in
    armv7)
        cat "$KOUT"/arch/arm/boot/zImage "$KOUT"/arch/arm/boot/dts/"$dtb".dtb > "$KOUT"/arch/arm/boot/zImage-dtb
        kernel_image="$KOUT/arch/arm/boot/zImage-dtb"
        ;;
    aarch64)
        cat "$KOUT"/arch/arm64/boot/Image.gz "$KOUT"/arch/arm64/boot/dts/"$dtb".dtb > "$KOUT"/arch/arm64/boot/Image.gz-dtb
        kernel_image="$KOUT/arch/arm64/boot/Image.gz-dtb"
        ;;
    *)
        echo "ERROR: Architecture $deviceinfo_arch is not supported!"
        exit 1
        ;;
esac

if [ "$deviceinfo_bootimg_mtk_mkimage" == "true" ]; then
    mv "$KOUT"/arch/arm/boot/zImage-dtb "$KOUT"/arch/arm/boot/zImage-dtb.orig
    mtk_mkimage.sh KERNEL "$KOUT"/arch/arm/boot/zImage-dtb.orig "$KOUT"/arch/arm/boot/zImage-dtb
fi

# Read the cmdline for the device from a file
cmdline=$(cat "$DIR"/files/"$device".cmdline)

mkdir -p $KOUT/

ramdisk="$DIR"/files/ramdisk-"$device".cpio.gz

# Copy a module and all its dependencies into the ramdisk
function copy_module {
    module=$1
    results=($(echo "$all_modules" | grep "/$module.ko" || true))
    if [ ${#results[@]} -ne 1 ]; then
        echo "Didn't find one result for '$module.ko': ${results[@]}"
        exit 1
    fi

    dst=$KOUT/ramdisk/lib/modules/$KERNELRELEASE/kernel/${results[0]}
    mkdir -p $(dirname $dst)
    cp ${results[0]} $dst

    depends_str=$(modinfo --field=depends ${results[0]})
    depends=(${depends_str//,/ })

    for depend in "${depends[@]}"; do
        copy_module $depend
    done
}

function handle_ramdisk_modules() {
    # Remove existing modules and create path for new ones
    KERNELRELEASE=$(cat "$KOUT"/include/config/kernel.release)
    rm -rf $KOUT/ramdisk/lib/modules
    mkdir -p $KOUT/ramdisk/lib/modules/$KERNELRELEASE/kernel/

    cp "$KOUT"/modules.order "$KOUT"/modules.builtin "$KOUT"/modules.builtin.modinfo $KOUT/ramdisk/lib/modules/$KERNELRELEASE/

    # Get all modules in source tree for later operation
    all_modules="$(find . -path "./$KOUT/*" -name "*.ko")"

    if [ -n "$modules" ]; then
        echo "Copying modules: $modules"
    else
        echo "Copying no modules."
    fi
    for module in $modules; do
        copy_module $module
    done

    # Generate modules.dep and map files
    depmod -b $KOUT/ramdisk $KERNELRELEASE

    echo -e "\n# Appended by boot-from-sourcetree.sh" >> $KOUT/ramdisk/etc/deviceinfo
    if [ "$no_module_load" -eq 0 ]; then
        echo "deviceinfo_modules_initfs=\"$modules\"" >> $KOUT/ramdisk/etc/deviceinfo
    else
        echo "deviceinfo_modules_initfs=\"\"" >> $KOUT/ramdisk/etc/deviceinfo
    fi
}

function handle_ramdisk_hooks() {
    # Remove existing hooks
    rm -f $KOUT/ramdisk/hooks/*

    if [ -z "$hook" ]; then
        echo "No hooks specified."
    else
        echo "Hook: $hook"
        cp -v "$pmaports_dir"/main/postmarketos-mkinitfs-hook-$hook/*.sh $KOUT/ramdisk/hooks/
    fi
}

function handle_ramdisk() {
    # Extract original ramdisk
    # TODO: Breaks with MTK ramdisk header
    mkdir -p $KOUT/ramdisk
    pushd $KOUT/ramdisk >/dev/null
    cat $ramdisk | zstd -d - | cpio --extract --quiet
    popd >/dev/null

    handle_ramdisk_modules
    handle_ramdisk_hooks

    # Repack ramdisk
    pushd $KOUT/ramdisk >/dev/null
    rm -f ../ramdisk.cpio.gz
    find . -not -path "./.git/*" | cpio --quiet -o -H newc | gzip > ../ramdisk.cpio.gz
    popd >/dev/null

    if [ "$deviceinfo_bootimg_mtk_mkimage" == "true" ]; then
        mv $KOUT/ramdisk.cpio.gz $KOUT/ramdisk.cpio.gz.orig
        mtk_mkimage.sh ROOTFS $KOUT/ramdisk.cpio.gz.orig $KOUT/ramdisk.cpio.gz
    fi

    ramdisk=$KOUT/ramdisk.cpio.gz
}

handle_ramdisk

mkbootimg \
    --base "$deviceinfo_flash_offset_base" \
    --pagesize "$deviceinfo_flash_pagesize" \
    --kernel_offset "$deviceinfo_flash_offset_kernel" \
    --ramdisk_offset "$deviceinfo_flash_offset_ramdisk" \
    --second_offset "$deviceinfo_flash_offset_second" \
    --tags_offset "$deviceinfo_flash_offset_tags" \
    --cmdline "$cmdline" \
    --kernel "$kernel_image" \
    --ramdisk "$ramdisk" \
    $deviceinfo_bootimg_custom_args \
    -o $KOUT/boot.img

echo SUCCESS: $KOUT/boot.img
