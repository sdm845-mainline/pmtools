# Utils

Utilities to run on host.

## `mkbootimg.sh`

Create an Android boot image, guessing values from your pmbootstrap config by default.

Will build the initramfs source in the `initrd` subdirectory.