#!/bin/bash

# Source the deviceinfo file for the current pmos device
PMAPORTS_PATH="$(pmbootstrap config work)"/cache_git/pmaports


## TODO
TOOLSDIR="$(dirname "$0")/.."
THIS="$(basename "$0")"

# shellcheck disable=SC2154
DTB="$KERNEL_OUTDIR/arch/arm64/boot/dts/${deviceinfo_dtb}.dtb"
IMAGE="$KERNEL_OUTDIR/arch/arm64/boot/Image.gz"
RAMDISK=""
OUT="$HOME/pmos/mainline-boot.img"
# SDM845 defaults
#CMDLINE="rootfs_part=sda14 rootfs_path=.stowaways/postmarketOS.img"


die() {
    echo "$1" 1>&2
    exit
}

usage() {
    echo "$THIS [ -d DTB(=$DTB) ] [ -r RAMDISK] [ -o OUT(=$OUT) ] [ -k KERNEL_IMAGE ] [ -c CMDLINE ] [ -t ] device"
	echo "The default RAMDISK reads the \"rootfs_part\" and \"rootfs_path=\" options to determine where
		the rootfs is. See https://gitlab.com/sdm845-mainline/pmtools#booting for more info."
    exit 1
}

while getopts "r:o:d:k:c:e:t:h" options; do
	case "${options}" in
		r)
			RAMDISK=${OPTARG}
			CUSTOM_RD=true
			;;
		o)
			OUT=${OPTARG}
			;;
		d)
			DTB=${OPTARG}
			;;
		k)
			IMAGE=${OPTARG}
			;;
		c)
			CMDLINE_EXTRA=${OPTARG}
			;;
		h)
			usage
			;;
		t) # Header version 2 and use a dt.img
			DTIMG_PATH=${OPTARG}
			;;
		:)
			die "-${OPTARG} requires an argument"
			;;
		*)
			usage
			;;
	esac
done

DEVICE=${@:$OPTIND:1}
[[ -z "$DEVICE" ]] && usage

DEVICEINFO_FILE="$(find "$PMAPORTS_PATH"/device -name "device-${DEVICE}")"/deviceinfo
[[ ! -f "$DEVICEINFO_FILE" ]] && echo "Can't find deviceinfo file for $DEVICE" && exit 1

# shellcheck disable=SC1090
source "$DEVICEINFO_FILE"

if [[ -n "$DTIMG_PATH" ]] && [[ -n "$DTB" ]]; then
	die "Can't set appended dtb and dt.img"
fi

[[ -f "$IMAGE" ]] || die "Kernel image \"$IMAGE\" doesn't exist"
if ! [[ "$DTB" = "" ]] || [[ "$DTB" = "/dev/null" ]] || [[ -z "$DTIMG_PATH" ]]; then
	[[ -f "$DTB" ]] || die "Device Tree Blob \"$DTB\" doesn't exist"

	echo -e "$THIS: Appending dtb \"$DTB\" to image \"./$IMAGE\""
	cat "$IMAGE" "$DTB" > /tmp/kernel-dtb || die "Failed to append dtb"
else
	cp "$IMAGE" /tmp/kernel-dtb
fi

CMDLINE="$CMDLINE $CMDLINE_EXTRA"

# shellcheck disable=SC2154
CMD="mkbootimg \
	--base '$deviceinfo_flash_offset_base' \
	--kernel_offset '$deviceinfo_flash_offset_kernel' \
	--ramdisk_offset '$deviceinfo_flash_offset_ramdisk' \
	--tags_offset '$deviceinfo_flash_offset_tags' \
	--pagesize '$deviceinfo_flash_pagesize' \
	--second_offset '$deviceinfo_flash_offset_second' \
	--cmdline \"$CMDLINE\" \
	--os_patch_level 2019-09-21 \
	--kernel /tmp/kernel-dtb -o '$OUT'"

#if [ -z "$CUSTOM_RD" ]; then
#	"$TOOLSDIR/initrd/build.sh" "$RAMDISK"
#fi

if [ ! -e "$RAMDISK" ] && [ -n "$RAMDISK" ]; then
	echo "$RAMDISK not found"
	exit 1
fi

if [ -n "$RAMDISK" ]; then
	CMD="$CMD --ramdisk $RAMDISK"
fi

if [ -n "$DTIMG_PATH" ]; then
	echo "Using dt.img"
	CMD="$CMD --header_version 2 --dtb '$DTIMG_PATH'"
fi

echo "$CMD"
# shellcheck disable=SC2086
eval $CMD || echo "Failed to make $OUT"
echo "$THIS: Boot image at $OUT"
