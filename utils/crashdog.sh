#!/usr/bin/zsh

source $HOME/pmos/tools/automation.sh && exec 3>&1


# Continually reboots device until it crashed
reboot_count=0
sleep_count=0

# Wait some predetermined amount of time then tell the device to reboot
function wait_then_reboot() {
	at-wait_booted
	# Wait for modem to be started (and crashed)
	sleep 7
	if ! ssht 'echo hello world'; then
		sleep_count=101
		return
	fi
	at-to_bootloader
	fastboot set_active a
	fastboot set_active b
	fastboot reboot
	#ssht 'sudo reboot -f >/dev/null &'
	echo "Reboot command sent"
	((reboot_count=reboot_count+1))
	sleep_count=0
}

while true; do
	sleep 0.2
	if $(ip a | grep -q "172.16.42"); then
		echo "DEVICE BOOTED: rcount: $reboot_count, scount: $sleep_count"
		wait_then_reboot;
		sleep 5;
	fi
	if [ $sleep_count -gt 100 ]; then
		echo "scount: $sleep_count"
		echo "Device seems to have crashed after $reboot_count reboots"
		exit 1
	fi;
	((sleep_count=sleep_count+1))
done
