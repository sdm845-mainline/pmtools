#!/usr/bin/zsh

. $HOME/pmos/tools/pmenv

SUCCESS_STRING="BISECT_SUCCESS"
FAIL_STRING="OpenRC 0.48 is starting"

#rrst bootloader &

cat /tmp/6mq-bisect/* | patch -p1
mm olddefconfig
mm

~/pmos/tools/boot-from-sourcetree.sh -o .output oneplus-enchilada
fastboot boot out/boot.img
timeout 1m rrst test "$SUCCESS_STRING" "$FAIL_STRING"
RESULT=$?
if [ $RESULT -eq 124 ]; then
	echo "Timed out"
	git reset --hard HEAD
	sleep 99999
fi
git reset --hard HEAD
exit $RESULT
