Compiling and flashing mainline for the OnePlus 6(T)
---

### This build guide is deprecated, [Please see the new, generic guide here](https://wiki.postmarketos.org/wiki/SDM845_Mainlining)

> **NOTE:** In this guide I may refer to "OnePlus 6" or "Enchilada", The OnePlus 6 (Enchilada) and OnePLus 6T (Fajita) have very very minor differences, where relevant I will point this out, otherwise these instructions can be followed for both.

## OnePlus 6 and 6T differences

* Display - 6T has different model and resolution
    * Enchilada - samsung_sofef00 - 1080x2280
    * Fajita - samsung_s6e3fc2x01 - 1080x2340
As a result 2 different device specific DTBs are created for the different displays: [enchilada](https://gitlab.com/sdm845-mainline/sdm845-linux-next/-/blob/oneplus-sdm845/arch/arm64/boot/dts/qcom/sdm845-oneplus-enchilada.dts) / [fajita](https://gitlab.com/sdm845-mainline/sdm845-linux-next/-/blob/oneplus-sdm845/arch/arm64/boot/dts/qcom/sdm845-oneplus-fajita.dts)
* Fingerprint reader - 6T has an in screen fingerprint reader, this will most likely require different handling if FP reader support ever gets implemented.

## Treble
Treble is both a blessing and a curse, the OnePlus 6 has 2 of system, vendor, boot and a few other partitions, this means it's super easy to dual boot with android as only one slot is used at any one time, simply `fastboot --set-active=a/b` to change!
For this guide I will be installing Linux to slot a, wherever you see a `_a` suffix after a partition, replace it with whichever slot you would like to install for, I recommend using not the one you currently have Android on if you plan on daily driving like I do.

Check you current slot by booting TWRP and going into the reboot menu.
Be aware that if you flash OTAs they are flashed to the inactive slot, if that slot has Linux on it, it doesn't anymore!

## Initial booting

By copying the [`sdm845-mtp.dts`](https://gitlab.com/sdm845-mainline/sdm845-linux-next/-/blob/oneplus-sdm845/arch/arm64/boot/dts/qcom/sdm845-mtp.dts) into a new `sdm845-enchilada.dts` file, removing the MDSS and MDSS_MDP nodes and a few others that were not required.

It was then possible to add a `simplefb` framebuffer node using the framebuffer created by the bootloader, essentially you're informing the kernel that a specific memory region is actually a display, and defining how it's encoded, see [here](https://gitlab.com/sdm845-mainline/sdm845-linux-next/-/commit/69421d2967bc107f7384e40a348fd3594129d989#254727b611d020b3dae1f6ef7b36cacb184c98c9_0_28).

## Making a boot image
It was a little daunting to figure out how to make a boot image, luckily [I've done most of the hard work for you](/mkbootimg.sh) in the `mkbootimg.sh` script.

### Dependancies

The require binaries are:
* <binutils>
* aarch64-linux-gnu-gcc
* fastboot
* mkbootimg
... And possibly a few others

#### Arch
```sh
sudo pacman -S aarch64-linux-gnu-gcc android-tools
```

#### Ubuntu
```sh
sudo apt install gcc-aarch64-linux-gnu android-tools-fastboot android-tools-mkbootimg
```

#### Tools

First you'll need to clone the kernel and this repo, unfortunately there are currently some hard coded references to `$HOME/pmos`:
```sh
cd $HOME
mkdir -p pmos && cd pmos
git clone https://gitlab.com/sdm845-mainline/pmtools tools
git clone https://gitlab.com/sdm845-mainline/sdm845-linux-next -b oneplus-sdm845 linux-next
```
It is relatively simple to build, source the [`pmenv.sh`](/pmenv.sh) file, you may want to open it for reference.

```sh
source tools/pmenv.sh
cd linux-next
m defconfig
m
```

This will compile a kernel that will boot just fine on your OnePlus 6 :D however there are a few extra steps needed to turn it into a boot image.
You will need an initramfs to boot into postmarketos, as you'll want a postmarketos image anyway to boot into, the quicket solution is as follows:

[Setup pmboostrap](https://wiki.postmarketos.org/wiki/Installation_guide)
Run `pmbootstrap init` and pick your device.

Choose whichever UI you would like, it doesn't matter for this step.

Now run
```
pmbootstrap export
```

This will generate an initramfs that we can use to boot.
Copy it somewhere useful so it doesn't get deleted when you reboot.
```sh
cd $HOME/pmos
cp $(readlink /tmp/postmarketOS-export/initramfs-oneplus-sdm845) initramfs-oneplus-sdm845
```

You can now append the device tree blob to the kernel, and build it into a boot image:

```sh
cd $HOME/pmos/linux-next/
# Replace enchilada with fajita as appropriate
cat .output/arch/arm64/boot/Image.gz .output/arch/arm64/boot/dts/qcom/sdm845-oneplus-enchilada.dtb > /tmp/kernel-dtb
mkbootimg \
	--base 0x0 \
	--kernel_offset 0x8000 \
	--ramdisk_offset 0x1000000 \
	--tags_offset 0x100 \
	--pagesize 4096 \
	--second_offset 0xf00000 \
    --ramdisk $HOME/pmos/initramfs-oneplus-sdm845 \
    --kernel /tmp/kernel-dtb -o $HOME/pmos/mainline-boot.img
```

A much simpler solution to the above is to just use the `mkbootimg.sh` script:
```
cd $HOME/pmos/linux-next
mkb -r $HOME/pmos/initramfs-oneplus-sdm845
```

Use `mkb -h` for guidance as it will default to the enchilada dtb image.

You also need to erase the dtbo partition, otherwise the bootloader will break boot by trying to inject Android dtbs and your phone will not leave fastboot mode.

```
fastboot erase dtbo
```

You will also need to build and flash a rootfs using the instructions [here](https://gist.github.com/calebccff/80bba8237e2ba512dc07f907e55b4891) if you haven't already.

> **TODO:** Move install instructions to this repo.

Unfortunately, I haven't yet found a way to avoid building the kernel during this process, I'd recommend copying the rootfs images to somewhere else once they're built so you don't have to run it again.

You're now ready to boot into postmarketos using your own kernel!
```
cd $HOME/pmos
fastboot boot mainline-boot.img
```

The current kernel in the pmaports repo is based on Linux 5.7-rc6, but the oneplus-sdm845 branch is based on 5.8-rc1 instead, running `uname -arn` on the device should give `5.8-rc1`.