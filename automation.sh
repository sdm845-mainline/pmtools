# shellcheck shell=bash
# Utils for automating the development cycle on a pmos device

# This should be the user@hostname for your dev target
# In this case I have an ssh config for pmos
# Set this in your environment, or leave the default

AT_LOGGING=true
AT_LOGFILE="/tmp/automation-devboard.log"

# shellcheck disable=SC2034
HOST_IP=172.16.42.2
TARGET_IP=172.16.42.1
TARGET_USER="user"

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
CYAN='\033[1;36m'
PURPLE='\033[0;35m'
NC='\033[0m'


# Disables "warning: permanently added host"
# and doesn't hang if the device disconnects
ssht() {
	SSH_TARGET=${TARGET_USER}@${TARGET_IP}
	# shellcheck disable=SC2068
	ssh -t "$SSH_TARGET" -o LogLevel=ERROR -o "ServerAliveInterval 1" -o "ServerAliveCountMax 2" $@
}
#alias fslot='fastboot getvar current-slot 2>&1 | grep "current" | cut -d" " -f2'
#alias fserial='fastboot getvar serialno 2>&1 | grep "current" | cut -d" " -f2'

# $1: fastboot var to get
at-fvar() {
	fastboot getvar "$1" | grep "$1" | cut -d" " -f2
}

fslot() {
	at-fvar current-slot
}

fbat() {
	at-fvar battery-voltage
}

fserial() {
	at-fvar serialno
}

atlog() {
	if [ "$AT_LOGGING" = true ]; then
		echo "${GREEN}at:${NC} $*" # | tee -a "$AT_LOGFILE"
	fi
}

at-tail_log() {
	[[ ! -f "$AT_LOGFILE" ]] && touch "$AT_LOGFILE"
	tail -F "$AT_LOGFILE"
}

# Get target state
# Possible return values:
# 'booted'
# 'bootloader'
# 'adb'
# 'nc' (not connected)
at-state() {
	if ! [ $(echo "$TARGET_IP" | grep -q 172.16.42.1) ]; then
		echo "booted"
		return
	fi
	if (ip a | grep -q 172.16.42.2); then
		echo "booted"
		return
	elif [ "$(fastboot devices | wc -l)" -gt 0 ]; then
		echo "bootloader"
		return
	elif [ "$(adb devices | wc -l)" -gt 2 ]; then
		echo "adb"
		return
	fi
	echo 'nc'
}

# $1: state to wait for
at-wait() {
	atlog "Waiting for state $1"
	while [ "$(at-state)" != "$1" ]; do
		sleep 0.5
	done
}

# Wait for device to boot
# FIXME: doesn't call at-state(), probably borked some stuff
# one of the commands in at-state() hangs :(
at-wait_booted() {
	at-wait "booted"
	atlog "Device booted, waiting for ssh"
	# Device is up, so safe to just attempt to connect
	# until sshd is up
	while ! (echo -e "\n" | nc "$TARGET_IP" 22 2> /dev/null | grep -q "OpenSSH"); do
		sleep 0.2
	done
	atlog "SSH daemon runnning on device"
}

at-wait_adb() {
	at-wait "adb"
	atlog "Device up on ADB"
}

# Run neofetch if available
# otherwise 'uname -a'
at-version() {
	atlog "Gettting target version info"
	echo "
		[[ \$(which neofetch) ]] && neofetch || uname -a
	" | ssht
}

at-nosudo() {
	atlog "Disabling sudo on target"
	ssht "sudo sed -i 's/%wheel ALL=(ALL) ALL/%wheel ALL=(ALL) NOPASSWD: ALL/g' /etc/sudoers" && atlog "Done"
}

at-usbnet() {
	atlog "Enabling USB networking on target"
	echo "
		sudo ip route add default via 172.16.42.2
		echo nameserver 1.1.1.1 | sudo tee /etc/resolv.conf > /dev/null
	" | ssht
}

# Run a command every time a device becomes available.
# Usually this should be a command like 'dmesg -w' or 'watch xyz'
at-watch() {
	[[ -z $* ]] && echo "You must provide a command to run!" && return 1
	atlog "Waiting for device to be booted"
	while : ; do
		at-wait_booted
		# shellcheck disable=SC2068
		ssht $@
		if [ $? -eq 0 ]; then
			break
		fi
		atlog "Device disconnected, press Ctrl-C to exit"
		sleep 2
	done
}

at-dmesg() {
	at-watch dmesg -w
}

# Install the reboot-mode package on pmos
# as it's needed for a lot of stuff here
at-install_reboot_mode() {
	atlog "Installing reboot-mode package"
	echo "
		sudo apk add reboot-mode
	" | ssht
}

at-setup() {
	atlog "Performing fresh-install setup on target"
	ssh-copy-id $SSH_TARGET
	at-nosudo
	at-install_reboot_mode
	atlog "Target is ready for use with 'at'"
	at-version
}

at-rst() {
	echo reset >> /tmp/rrst
}

at-bl() {
	echo bootloader >> /tmp/rrst
	at-wait "bootloader"
}

# Reboot device, with support for bootloader and recovery mode
# $1: [bootloader|recovery]
# $2: wait for for device to be detected
at-reboot() {
	state=$(at-state)
	# Make sure target is connected and booted
	if [ "$state" = "booted" ]; then
		at-wait_booted
		if [ -z "$1" ]; then
			atlog "Rebooting"
			ssht sudo reboot -f
			return
		fi
		atlog "Rebooting to '$1'"
		if [ ! "$(ssht which reboot-mode)" ]; then
			echo "reboot-mode package missing, installing..."
			at-install_reboot_mode
		fi
		# Some trickery to stop ssh getting stuck here
		echo "
			sudo reboot-mode '$1' &
			exit
		" | ssht
	elif [ "$state" = "adb" ]; then
		adb reboot "$1"
	elif [ "$state" = "bootloader" ]; then
		fastboot reboot "$1"
	fi
	if [ "$1" = "nc" ] && [ "$2" = "true" ]; then
		# fastboot will wait for the device to appear
		fslot > /dev/null
	fi
}

# Put the device in bootloader mode
# returns when the device is in bootlaoder
# mode
at-to_bootloader() {
	state="$(at-state)"
	while [ "$state" = "nc" ]; do
		sleep 0.2
		state="$(at-state)"
	done
	atlog "Device in state: $state"
	slot=""
	if [ "$state" = "booted" ]; then
		at-reboot bootloader
	elif [ "$state" = "adb" ]; then
		adb reboot bootloader
	fi
	slot="$(fslot)"

	atlog "Current slot is $slot"
	if [ "$state" = "nc" ]; then
		return 1 # device not connected
	fi
}

# Flash a boot image
# $1: <path> the android boot image to flash
# $2: [true|false] reboot after flash
at-flash() {
	[[ -z "$1" ]] && echo "Please provide a boot image to flash" && return 1
	at-to_bootloader
	fastboot flash boot "$1"
	if [ "$2" != "false" ]; then
		fastboot reboot
	fi
}
