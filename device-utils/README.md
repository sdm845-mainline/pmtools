# Device Utils

Scripts to be executed on-device.

## `creategadget.sh`

Run as root, sets up a USB network gadget device, add to a service file to run on boot.

## `usbnet.sh`

Add an IP route via USB gadget to host and default DNS to resolv.conf