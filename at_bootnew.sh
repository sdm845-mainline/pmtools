#!/usr/bin/bash

set -ex

trap_exit() {
    echo "Exiting..."
    kill -9 $TAIL_PID || true
    exit 0
}

trap trap_exit EXIT

SSH_TARGET="user@172.16.42.1"
source /home/cas/pmos/tools/automation.sh

# pmbootstrap config > /tmp/pmb.cfg

# pmbootstrap() {
#     /usr/bin/pmbootstrap --details-to-stdout -c /tmp/pmb.cfg "$@"
# }

# pmbootstrap config device shift-axolotl
# pmbootstrap config extra_packages none
# pmbootstrap config build_pkgs_on_install False
# pmbootstrap config ui phosh
# # Set up mirror with the kernel package that was just built in CI
# pmbootstrap config mirrors_postmarketos "https://connolly.tech/autotester/,http://mirror.postmarketos.org/postmarketos/"

# # mehhhh
# keys_dir="$(pmbootstrap config work)/chroot_rootfs-$(pmbootstrap config device)/etc/apk/keys"
# mkdir -p $keys_dir
# curl https://connolly.tech/autotester/master/connolly.tech.pub > $keys_dir/connolly.tech.rsa.pub

# # Assuming pmaports is up to date this should do everything!
# pmbootstrap export

tail -F /tmp/uart.out &
TAIL_PID=$!

[[ $(fastboot devices | wc -l) -eq 0 ]] && echo bootloader >> /tmp/rrst
curl https://connolly.tech/autotester/master/boot.img > /tmp/boot.img
fastboot boot /tmp/boot.img

set +x
at-wait_booted
set -x

sleep 10

kill -9 $TAIL_PID

echo "
    lsmod
    ps axu
" | ssht

ssht dmesg > /tmp/dmesg
echo "\n\nINFO: dmesg"
cat /tmp/dmesg
